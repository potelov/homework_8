package ru.fintech.homework.ui

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import ru.fintech.homework.data.Repository
import ru.fintech.homework.ui.detail.DetailViewModel
import ru.fintech.homework.ui.main.MainViewModel
import javax.inject.Inject

class ViewModelFactory @Inject constructor(private val repository: Repository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(MainViewModel::class.java) -> MainViewModel(repository) as T
            modelClass.isAssignableFrom(DetailViewModel::class.java) -> DetailViewModel(repository) as T
            else -> throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}