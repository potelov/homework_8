package ru.fintech.homework.ui

import ru.fintech.homework.data.database.NodeWithAllNode
import ru.fintech.homework.data.database.NodeWithChild
import ru.fintech.homework.data.database.NodeWithParent

interface Callbacks {
    fun onItemClicked(node: NodeWithAllNode)
    fun onChildItemClick(node: NodeWithChild)
    fun onParentItemClick(node: NodeWithParent)
}