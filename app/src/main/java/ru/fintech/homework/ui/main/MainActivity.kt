package ru.fintech.homework.ui.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.View
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import ru.fintech.homework.App
import ru.fintech.homework.R
import ru.fintech.homework.data.database.Node
import ru.fintech.homework.data.database.NodeWithAllNode
import ru.fintech.homework.data.database.NodeWithChild
import ru.fintech.homework.data.database.NodeWithParent
import ru.fintech.homework.ui.*
import ru.fintech.homework.ui.detail.DetailActivity
import ru.fintech.homework.utils.TYPE_NODE
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: GenericAdapter<Any>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        App.appComponent.inject(this)
        setupFab()
        setupRecycler()
        setUp()
    }

    private fun setUp() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        viewModel.screenState.observe(this, Observer<ScreenState> { screenState ->
            when (screenState) {
                is ScreenState.Nodes -> {
                    if (screenState.data.isEmpty()) {
                        tv_welcome.visibility = View.VISIBLE
                    } else {
                        tv_welcome.visibility = View.GONE
                        adapter.setItems(screenState.data)
                    }
                }
                is ScreenState.Message -> {
                    Toast.makeText(this, screenState.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
        viewModel.getNodes()
    }

    private fun setupFab() {
        findViewById<FloatingActionButton>(R.id.fab_refresh).apply {
            setOnClickListener {
                showDialog()
            }
        }
    }

    private fun setupRecycler() {
        adapter = object : GenericAdapter<Any>() {
            override fun getLayoutId(position: Int, obj: Any): Int {
                return TYPE_NODE
            }
            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                return ViewHolderFactory.create(view, viewType, callBacks)
            }

        }
        recycler.adapter = adapter
        recycler.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }

    private fun openDetailActivity(node: Node) {
        val intent = DetailActivity.getStartIntent(this, node)
        startActivity(intent)
    }

    private val callBacks: Callbacks = object : Callbacks {
        override fun onItemClicked(node: NodeWithAllNode) {
            openDetailActivity(node.node)
        }

        override fun onChildItemClick(node: NodeWithChild) {
            // do nothing
        }

        override fun onParentItemClick(node: NodeWithParent) {
            // do nothing
        }
    }

    private fun showDialog() {
        val alert = AlertDialog.Builder(this)
        val editText = EditText(this)
        editText.inputType = InputType.TYPE_CLASS_NUMBER
        alert.setTitle(getString(R.string.dialog_title))
        alert.setView(editText)
        alert.setPositiveButton(getString(R.string.dialog_add_node)) { _, _ ->
            val text = editText.text.toString()
            if (text.isNotEmpty()) {
                viewModel.addNode(Node(0, text.toLong()))
            } else {
                Toast.makeText(this, "Нужно ввести число", Toast.LENGTH_SHORT).show()
            }
        }
        alert.setNegativeButton(getString(R.string.dialog_cancel)) { dialog, _ ->
            dialog.cancel()
        }
        alert.show()
    }
}