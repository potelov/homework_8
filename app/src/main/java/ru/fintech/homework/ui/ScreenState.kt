package ru.fintech.homework.ui

import ru.fintech.homework.data.database.NodeWithAllNode
import ru.fintech.homework.data.database.NodeWithChild
import ru.fintech.homework.data.database.NodeWithParent

sealed class ScreenState {
    data class Message(val message: Int) : ScreenState()
    data class Nodes(val data: List<NodeWithAllNode>) : ScreenState()
    data class ParentNode(val data: List<NodeWithParent>) : ScreenState()
    data class ChildNode(val data: List<NodeWithChild>) : ScreenState()
}

