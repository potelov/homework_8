package ru.fintech.homework.ui

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.item_node.view.*
import ru.fintech.homework.R
import ru.fintech.homework.data.database.NodeWithAllNode
import ru.fintech.homework.data.database.NodeWithChild
import ru.fintech.homework.data.database.NodeWithParent
import ru.fintech.homework.utils.TYPE_CHILD
import ru.fintech.homework.utils.TYPE_NODE
import ru.fintech.homework.utils.TYPE_PARENT


object ViewHolderFactory {

    fun create(view: View, viewType: Int, callbacks: Callbacks): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_NODE -> NodeViewHolder(view, callbacks)
            TYPE_CHILD -> ChildViewHolder(view, callbacks)
            TYPE_PARENT -> ParentViewHolder(view, callbacks)
            else -> {
                NodeViewHolder(view, callbacks)
            }
        }
    }

    class NodeViewHolder(itemView: View, private val callbacks: Callbacks) : RecyclerView.ViewHolder(itemView), GenericAdapter.Binder<NodeWithAllNode> {
        private var textView: TextView = itemView.findViewById(R.id.tv_value)
        override fun bind(data: NodeWithAllNode) {
            textView.text = data.node.value.toString()
            if (data.childList.isNotEmpty() && data.parentList.isNotEmpty()) {
                setBackgroundColor(R.color.red, textView, itemView)
            } else if (data.parentList.isNotEmpty()) {
                setBackgroundColor(R.color.blue, textView, itemView)
            } else if (data.childList.isNotEmpty()) {
                setBackgroundColor(R.color.yellow, textView, itemView)
            } else {
                setBackgroundColor(R.color.white, textView, itemView)
            }
            itemView.setOnClickListener { callbacks.onItemClicked(data) }
        }
    }

    class ParentViewHolder(itemView: View, private val callbacks: Callbacks) : RecyclerView.ViewHolder(itemView), GenericAdapter.Binder<NodeWithParent> {

        private var textView: TextView = itemView.findViewById(R.id.tv_value)
        override fun bind(data: NodeWithParent) {
            textView.text = data.node.value.toString()
            if (data.count > 0) {
                setBackgroundColor(R.color.green, textView, itemView)
            } else {
                setBackgroundColor(R.color.white, textView, itemView)
            }
            itemView.setOnClickListener { callbacks.onParentItemClick(data) }
        }
    }

    class ChildViewHolder(itemView: View, private val callbacks: Callbacks) : RecyclerView.ViewHolder(itemView), GenericAdapter.Binder<NodeWithChild> {

        private var textView: TextView = itemView.findViewById(R.id.tv_value)
        override fun bind(data: NodeWithChild) {
            textView.text = data.node.value.toString()
            if (data.count > 0) {
                setBackgroundColor(R.color.green, textView, itemView)
            } else {
                setBackgroundColor(R.color.white, textView, itemView)
            }
            itemView.setOnClickListener { callbacks.onChildItemClick(data) }
        }
    }

    private fun setBackgroundColor(colorId: Int, textView: TextView, view: View) {
        textView.tv_value.setBackgroundColor(ContextCompat.getColor(view.context, colorId))
    }
}
