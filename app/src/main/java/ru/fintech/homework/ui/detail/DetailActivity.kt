package ru.fintech.homework.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_detail.*
import ru.fintech.homework.R
import ru.fintech.homework.data.database.Node
import ru.fintech.homework.utils.TYPE_CHILD
import ru.fintech.homework.utils.TYPE_PARENT

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        node = intent.getSerializableExtra(EXTRA_INFO) as Node
        setUp()
    }

    private fun setUp() {
        tv_title.text = node!!.value.toString()
        vp_node.adapter = DetailAdapter(supportFragmentManager)
        tab_layout.setupWithViewPager(vp_node)
        iv_back.setOnClickListener { onBackPressed() }
    }

    companion object {
        private const val EXTRA_INFO = "EXTRA_INFO"

        fun getStartIntent(context: Context, node: Node): Intent {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(EXTRA_INFO, node)
            return intent
        }

        private var node: Node? = null

        private val INFO_TITLES = arrayOf(
            R.string.detail_parent_title,
            R.string.detail_child_title
        )
        private val INFO_PAGES = arrayOf(
            { DetailFragment.newInstance(node!!.id, TYPE_PARENT) },
            { DetailFragment.newInstance(node!!.id, TYPE_CHILD) }
        )
    }

    inner class DetailAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getCount() = INFO_PAGES.size

        override fun getItem(position: Int) = INFO_PAGES[position]()

        override fun getPageTitle(position: Int): CharSequence {
            return resources.getString(INFO_TITLES[position])
        }
    }
}