package ru.fintech.homework.ui.detail

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import ru.fintech.homework.R
import ru.fintech.homework.data.Repository
import ru.fintech.homework.data.database.NodeWithNode
import ru.fintech.homework.ui.ScreenState
import ru.fintech.homework.ui.base.BaseViewModel
import timber.log.Timber
import javax.inject.Inject

class DetailViewModel @Inject constructor(private val repository: Repository) : BaseViewModel() {

    private val _screenState: MutableLiveData<ScreenState> = MutableLiveData()
    val screenState: LiveData<ScreenState>
        get() = _screenState

    fun getNodeWithParent(id: Long) {
        compositeDisposable.add(repository.getNodeWithParent(id)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe(
                { data ->
                    _screenState.value = ScreenState.ParentNode(data)
                },
                { error ->
                    Timber.e(error)
                }
            ))
    }

    fun getNodeWithChild(id: Long) {
        compositeDisposable.add(repository.getNodeWithChild(id)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe(
                { data ->
                    _screenState.value = ScreenState.ChildNode(data)
                },
                { error ->
                    Timber.e(error)
                }
            ))
    }

    fun addRelation(nodeWithNode: NodeWithNode) {
        compositeDisposable.add(repository.addRelationWithNode(nodeWithNode)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe(
                { data ->
                    if (data) {
                        _screenState.value = ScreenState.Message(R.string.relation_add)
                    }
                },
                { error ->
                    Timber.e(error)
                }
            ))
    }

    fun removeRelation(nodeWithNode: NodeWithNode) {
        compositeDisposable.add(repository.removeRelationWithNode(nodeWithNode)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe(
                { data ->
                    if (data) {
                        _screenState.value = ScreenState.Message(R.string.relation_del)
                    }
                },
                { error ->
                    Timber.e(error)
                }
            ))
    }
}