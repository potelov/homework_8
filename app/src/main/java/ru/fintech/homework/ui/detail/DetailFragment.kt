package ru.fintech.homework.ui.detail

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_detail.*
import ru.fintech.homework.App
import ru.fintech.homework.R
import ru.fintech.homework.data.database.NodeWithAllNode
import ru.fintech.homework.data.database.NodeWithChild
import ru.fintech.homework.data.database.NodeWithNode
import ru.fintech.homework.data.database.NodeWithParent
import ru.fintech.homework.ui.*
import ru.fintech.homework.utils.TYPE_CHILD
import ru.fintech.homework.utils.TYPE_PARENT
import javax.inject.Inject

class DetailFragment : Fragment() {

    @Inject lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: DetailViewModel
    private lateinit var adapter: GenericAdapter<Any>

    companion object {
        private const val ARGUMENT_ID = "ARGUMENT_ID"
        private const val ARGUMENT_TYPE = "ARGUMENT_TYPE"

        private var nodeId: Long? = null
        private var nodeType: Int? = null

        fun newInstance(id: Long, type: Int): DetailFragment {
            val args = Bundle()
            args.putLong(ARGUMENT_ID, id)
            args.putInt(ARGUMENT_TYPE, type)
            val fragment = DetailFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupRecycler()
        setupViewModel()
        setUp()
    }

    private fun setUp() {
        nodeId = arguments?.get(ARGUMENT_ID) as Long
        nodeType = arguments?.get(ARGUMENT_TYPE) as Int
        when (nodeType) {
            TYPE_PARENT -> viewModel.getNodeWithParent(nodeId!!)
            TYPE_CHILD -> viewModel.getNodeWithChild(nodeId!!)
        }
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailViewModel::class.java)
        viewModel.screenState.observe(this, Observer<ScreenState> { screenState ->
            when (screenState) {
                is ScreenState.ParentNode -> {
                    adapter.setItems(screenState.data)
                }
                is ScreenState.ChildNode -> {
                    adapter.setItems(screenState.data)
                }
                is ScreenState.Message -> {
                    Toast.makeText(context, screenState.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun setupRecycler() {
        adapter = object : GenericAdapter<Any>() {
            override fun getLayoutId(position: Int, obj: Any): Int {
                return when(obj){
                    is NodeWithChild -> TYPE_CHILD
                    is NodeWithParent -> TYPE_PARENT
                    else -> {
                        TYPE_CHILD
                    }
                }
            }
            override fun getViewHolder(view: View, viewType: Int): RecyclerView.ViewHolder {
                return ViewHolderFactory.create(view, viewType, callBacks)
            }

        }
        recycler.adapter = adapter
        recycler.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    }

    private val callBacks: Callbacks = object : Callbacks {
        override fun onChildItemClick(node: NodeWithChild) {
            showDialog(node)
        }

        override fun onParentItemClick(node: NodeWithParent) {
            showDialog(node)
        }

        override fun onItemClicked(node: NodeWithAllNode) {
            // do nothing
        }
    }

    private fun showDialog(node: Any) {
        val alert = AlertDialog.Builder(context!!)
        alert.setTitle(getString(R.string.dialog_title_second))
        alert.setPositiveButton(getString(R.string.dialog_add_node)) { _, _ ->
            addRelation(node)
        }
        alert.setNegativeButton(getString(R.string.dialog_del_node)) { _, _ ->
            removeRelation(node)
        }
        alert.show()
    }

    private fun addRelation(obj: Any) {
        when (obj) {
            is NodeWithChild -> viewModel.addRelation(NodeWithNode(0, obj.node.id, nodeId!!))
            is NodeWithParent -> viewModel.addRelation(NodeWithNode(0, nodeId!!, obj.node.id ))
        }
    }

    private fun removeRelation(obj: Any) {
        when (obj) {
            is NodeWithChild -> viewModel.removeRelation(NodeWithNode(0, obj.node.id, nodeId!!))
            is NodeWithParent -> viewModel.removeRelation(NodeWithNode(0, nodeId!!, obj.node.id ))
        }
    }
}