package ru.fintech.homework.utils

const val TYPE_CHILD = 1001
const val TYPE_PARENT = 1002
const val TYPE_NODE = 1003
const val DB_NAME = "node"