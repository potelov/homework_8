package ru.fintech.homework

import android.app.Application
import com.facebook.stetho.Stetho
import ru.fintech.homework.di.component.AppComponent
import ru.fintech.homework.di.component.DaggerAppComponent
import ru.fintech.homework.di.module.AppModule
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this)).build()
    }

    companion object {
        lateinit var appComponent: AppComponent
    }
}
