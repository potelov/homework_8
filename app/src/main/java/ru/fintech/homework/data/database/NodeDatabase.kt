package ru.fintech.homework.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import ru.fintech.homework.data.database.dao.NodeDao
import ru.fintech.homework.data.database.dao.NodeWithNodeDao

@Database(entities = [(Node::class), (NodeWithNode::class)], version = 1)
abstract class NodeDatabase : RoomDatabase() {

    abstract fun nodeDao(): NodeDao

    abstract fun nodeWithNodeDao(): NodeWithNodeDao

}