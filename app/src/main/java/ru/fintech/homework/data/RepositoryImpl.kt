package ru.fintech.homework.data

import io.reactivex.Flowable
import io.reactivex.Single
import ru.fintech.homework.data.database.*
import ru.fintech.homework.data.database.dao.NodeDao
import ru.fintech.homework.data.database.dao.NodeWithNodeDao
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RepositoryImpl @Inject constructor(
    private val nodeDao: NodeDao,
    private val nodeWithNodeDao: NodeWithNodeDao) : Repository {

    override fun getAllNodes(): Flowable<List<NodeWithAllNode>> {
        return nodeDao.getAllNodes()
    }

    override fun getNodeWithChild(id: Long): Flowable<List<NodeWithChild>> {
        return nodeDao.getNodeWithChild(id)
    }

    override fun getNodeWithParent(id: Long): Flowable<List<NodeWithParent>> {
        return nodeDao.getNodeWithParent(id)
    }

    override fun addNode(node: Node): Single<Boolean> {
        return Single.fromCallable {
            nodeDao.insert(node)
            true
        }
    }

    override fun addRelationWithNode(nodeWithNode: NodeWithNode): Single<Boolean> {
        return Single.fromCallable {
            nodeWithNodeDao.insert(nodeWithNode)
            true
        }
    }

    override fun removeRelationWithNode(nodeWithNode: NodeWithNode): Single<Boolean> {
        return Single.fromCallable {
            nodeWithNodeDao.deleteRelation(nodeWithNode.parentId, nodeWithNode.childId)
            true
        }
    }
}