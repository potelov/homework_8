package ru.fintech.homework.data.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import ru.fintech.homework.data.database.NodeWithNode

@Dao
abstract class NodeWithNodeDao : BaseDao<NodeWithNode> {

    @Query("DELETE FROM nodewithnode WHERE parentId = :parentId AND childId = :childId")
    abstract fun deleteRelation(parentId: Long, childId: Long)
}