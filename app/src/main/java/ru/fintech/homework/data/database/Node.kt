package ru.fintech.homework.data.database

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.Relation
import java.io.Serializable

@Entity
data class Node(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    var value: Long
) : Serializable

@Entity
data class NodeWithNode(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    var childId: Long,
    var parentId: Long
)

class NodeWithParent {
    @Embedded lateinit var node: Node
    var count: Int = 0
}

class NodeWithChild {
    @Embedded lateinit var node: Node
    var count: Int = 0
}

class NodeWithAllNode {
    @Embedded lateinit var node: Node
    @Relation(
        parentColumn = "id",
        entityColumn = "childId",
        entity = NodeWithNode::class
    )
    lateinit var parentList: List<NodeWithNode>
    @Relation(
        parentColumn = "id",
        entityColumn = "parentId",
        entity = NodeWithNode::class
    )
    lateinit var childList: List<NodeWithNode>
}




