package ru.fintech.homework.data.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import io.reactivex.Flowable
import ru.fintech.homework.data.database.Node
import ru.fintech.homework.data.database.NodeWithAllNode
import ru.fintech.homework.data.database.NodeWithChild
import ru.fintech.homework.data.database.NodeWithParent

@Dao
abstract class NodeDao : BaseDao<Node> {

    @Transaction
    @Query("SELECT * FROM node")
    abstract fun getAllNodes(): Flowable<List<NodeWithAllNode>>

    @Query("select n.*, p.count\n" +
            "from node n\n" +
            "left join (\n" +
            "\tselect parentId as id, count(*) as count \n" +
            "\tfrom nodewithnode\n" +
            "\tgroup by parentId\n" +
            ") p \n" +
            "on p.id = n.id\n" +
            "where n.id != :id\n" +
            "order by count desc")
    abstract fun getNodeWithParent(id: Long): Flowable<List<NodeWithParent>>

    @Query("select n.*, c.count\n" +
            "from node n\n" +
            "left join (\n" +
            "\tselect childId as id, count(*) as count \n" +
            "\tfrom nodewithnode\n" +
            "\tgroup by childId\n" +
            ") c \n" +
            "on c.id = n.id\n" +
            "where n.id != :id\n" +
            "order by count desc")
    abstract fun getNodeWithChild(id: Long): Flowable<List<NodeWithChild>>

}