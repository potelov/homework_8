package ru.fintech.homework.data

import io.reactivex.Flowable
import io.reactivex.Single
import ru.fintech.homework.data.database.*

interface Repository {

    fun getAllNodes(): Flowable<List<NodeWithAllNode>>

    fun getNodeWithChild(id: Long): Flowable<List<NodeWithChild>>

    fun getNodeWithParent(id: Long): Flowable<List<NodeWithParent>>

    fun addNode(node: Node): Single<Boolean>

    fun addRelationWithNode(nodeWithNode: NodeWithNode): Single<Boolean>

    fun removeRelationWithNode(nodeWithNode: NodeWithNode): Single<Boolean>
}
