package ru.fintech.homework.di.component

import dagger.Component
import ru.fintech.homework.di.module.AppModule
import ru.fintech.homework.di.module.RepositoryModule
import ru.fintech.homework.ui.detail.DetailFragment
import ru.fintech.homework.ui.main.MainActivity
import javax.inject.Singleton

@Component(modules = [AppModule::class, RepositoryModule::class])
@Singleton
interface AppComponent {

    fun inject(mainActivity: MainActivity)

    fun inject(detailFragment: DetailFragment)

}