package ru.fintech.homework.di.module

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import ru.fintech.homework.App
import ru.fintech.homework.data.Repository
import ru.fintech.homework.data.RepositoryImpl
import ru.fintech.homework.data.database.NodeDatabase
import ru.fintech.homework.ui.ViewModelFactory
import ru.fintech.homework.utils.DB_NAME
import javax.inject.Singleton

@Module
class AppModule(private val application: App) {

    @Provides
    @Singleton
    fun provideContext(): Context = application

    @Provides
    @Singleton
    fun provideViewModelFactory(repository: Repository): ViewModelFactory {
        return ViewModelFactory(repository)
    }

    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): NodeDatabase =
        Room.databaseBuilder(context, NodeDatabase::class.java, DB_NAME).build()

    @Provides
    @Singleton
    fun provideNodeDao(nodeDatabase: NodeDatabase): RepositoryImpl = RepositoryImpl(nodeDatabase.nodeDao(), nodeDatabase.nodeWithNodeDao())

}